package io.gatling.funspec.example

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class GatlingPredictionsApiTest extends Simulation {

  val httpConf = http.baseUrl("http://localhost:8083")
    .header("Accept", "application/json")
  val repeatCount = 10
  val random = scala.util.Random

  def getClient() = {
    repeat(repeatCount) {
      exec(http("Predictions API test")
        .get("/predictions/" + random.nextInt(63)+ "/30")
        .check(status.is(200)))
    }
  }

  val scn = scenario("Predictions API test")
    .exec(getClient())

  setUp(
    scn.inject(atOnceUsers(1))
  ).protocols(httpConf)

}