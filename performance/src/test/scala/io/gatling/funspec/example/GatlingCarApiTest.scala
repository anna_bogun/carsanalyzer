package io.gatling.funspec.example

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class GatlingCarApiTest extends Simulation {

  val httpConf = http.baseUrl("http://localhost:8083")
    .header("Accept", "application/json")
  val repeatCount = 10
  val random = scala.util.Random

  def getCar() = {
    repeat(repeatCount) {
      exec(http("Car API test")
        .get("/cars/" + random.nextInt(100))
        .check(status.is(200)))
    }
  }

  val scn = scenario("Car API test")
    .exec(getCar())

  setUp(
    scn.inject(atOnceUsers(1))
  ).protocols(httpConf)

}