package io.gatling.funspec.example

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class GatlingRecommendApiTest extends Simulation {

  val httpConf = http.baseUrl("http://localhost:8083")
    .header("Accept", "application/json")
  val repeatCount = 10
  val random = scala.util.Random

  def reccomend() = {
    repeat(repeatCount) {
      exec(http("Recommend API test")
        .get("/recommend/" + random.nextInt(63)+ "?count=30")
        .check(status.is(200)))
    }
  }

  val scn = scenario("Recommend API test")
    .exec(reccomend())

  setUp(
    scn.inject(atOnceUsers(1))
  ).protocols(httpConf)

}