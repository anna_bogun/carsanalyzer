package io.gatling.funspec.example

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.http.funspec.GatlingHttpFunSpec
import io.gatling.funspec.example.GatlingSwaggerTest._

class GatlingSwaggerTest extends GatlingHttpFunSpec {

  val baseUrl = "http://localhost:8083"
  override def httpProtocol = super.httpProtocol.header("MyHeader", "MyValue")

  spec {
    http("Swagger-ui test")
      .get("/swagger-ui.html#/")
      .check(status.is(200))
  }
}

object GatlingSwaggerTest {
}
