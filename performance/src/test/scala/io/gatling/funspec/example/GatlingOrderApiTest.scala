package io.gatling.funspec.example

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class GatlingOrderApiTest extends Simulation {

  val httpConf = http.baseUrl("http://localhost:8083")
    .header("Accept", "application/json")
  val repeatCount = 10
  val random = scala.util.Random

  def getOrder() = {
    repeat(repeatCount) {
      exec(http("Order API test")
        .get("/orders/" + random.nextInt(60))
        .check(status.is(200)))
    }
  }

  val scn = scenario("Order API test")
    .exec(getOrder())

  setUp(
    scn.inject(atOnceUsers(1))
  ).protocols(httpConf)

}