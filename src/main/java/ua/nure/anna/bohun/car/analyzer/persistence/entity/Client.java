package ua.nure.anna.bohun.car.analyzer.persistence.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

/**
 * User entity.
 **/
@Entity
@Table
@Data
public class Client implements BaseEntity {

    private static final long serialVersionUID = 9196011509781773061L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected int id;

    @Column(nullable = false, unique = true)
    private String login;
    @Column(nullable = false)
    private String password;
    @Column(nullable = false)
    private String email;
    @Column(nullable = false)
    private String phone;
    @Column(nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String lastName;
    @Column(nullable = false)
    private Boolean male;
    @Column(nullable = false)
    private Date birthdate;

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client")
    private List<Order> orders;
}