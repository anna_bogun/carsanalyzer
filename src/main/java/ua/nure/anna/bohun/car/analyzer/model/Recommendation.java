package ua.nure.anna.bohun.car.analyzer.model;

import lombok.Data;
import ua.nure.anna.bohun.car.analyzer.persistence.entity.CarModel;
import ua.nure.anna.bohun.car.analyzer.persistence.entity.Client;

import java.util.ArrayList;
import java.util.List;

@Data
public class Recommendation {

    private Integer clientId;

    private List<RecommendationKnowledge> recommendationList = new ArrayList<>();

    public void addRecommendation(RecommendationKnowledge recommendation) {
        recommendationList.add(recommendation);
    }

    public void addRecommendation(CarModel car, Boolean isRecommended) {
        recommendationList.add(new RecommendationKnowledge(car.getId(), isRecommended));
    }
}
