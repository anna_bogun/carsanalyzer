package ua.nure.anna.bohun.car.analyzer.persistence;

/**
 * This class contains all SQL queries used by application (DBManager uses these
 * queries). DBMS - MySQL. The schema name does not occur in queries, so it can
 * be any. Class has private constructor.
 **/

public final class Queries {

    /**
     * SQL queries to `user` table
     **/
    public static final String SQL_UPDATE_USER_ROLE = "UPDATE user SET role = ? WHERE id = ?";
    public static final String SQL_UPDATE_USER_BLOCKED = "UPDATE user SET blocked = ? WHERE id = ?";
    public static final String SQL_FIND_USER_BY_LOGIN = "SELECT * FROM user WHERE login = ?";
    public static final String SQL_FIND_ALL_USERS = "SELECT * FROM user";
    public static final String SQL_INSERT_USER = "INSERT INTO user "
            + " VALUES (DEFAULT, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 'client', DEFAULT, ?)";
    public static final String SQL_FIND_USER_BY_ID = "SELECT * FROM user WHERE id = ?";
    /**
     * SQL queries to `brand` table
     **/
    public static final String SQL_FIND_ALL_BRANDS = "SELECT * FROM brand";
    public static final String SQL_FIND_BRAND_BY_ID = "SELECT * FROM brand WHERE id = ?";
    /**
     * SQL queries to `parking` table
     **/
    public static final String SQL_FIND_ALL_PARKING = "SELECT * FROM parking";
    public static final String SQL_FIND_PARKING_BY_ID = "SELECT * FROM parking WHERE id = ?";
    /**
     * SQL queries to `quality_class` table
     **/
    public static final String SQL_FIND_ALL_QUALITY_CLASSES = "SELECT * FROM quality_class";

    /**
     * SQL queries to `car_description` table
     **/
    public static final String SQL_DELETE_CAR_DESCRIPTION_BY_ID = "DELETE FROM car_description WHERE id = ?";
    public static final String SQL_FIND_ALL_CAR_DESCRIPTIONS = "SELECT cd.id, brand_id, quality_class_id, "
            + " price, total_count, model, coupe_type, generation," + " power,  fuel_type, cd.description, cd.image,"
            + " b.name as brand_name, qs.name as quality_class_name"
            + " FROM car_description cd JOIN brand b ON cd.brand_id = b.id"
            + " JOIN quality_class qs ON cd.quality_class_id = qs.id ";
    public static final String SQL_FIND_CAR_DESCRIPTIONS_ORDER_BY_BRAND = SQL_FIND_ALL_CAR_DESCRIPTIONS
            + " ORDER BY brand_name";
    public static final String SQL_FIND_CAR_DESCRIPTIONS_ORDER_BY_PRICE = SQL_FIND_ALL_CAR_DESCRIPTIONS
            + " ORDER BY price";
    public static final String SQL_GET_CAR_DESCRIPTION_BY_ID = SQL_FIND_ALL_CAR_DESCRIPTIONS + " WHERE  cd.id = ?";
    public static final String SQL_FIND_CAR_DESCRIPTIONS = SQL_FIND_ALL_CAR_DESCRIPTIONS
            + " WHERE price >=? AND price<=? AND brand_id = ? AND quality_class_id = ? ";
    public static final String SQL_FIND_CAR_DESCRIPTIONS_BY_QS_ID = SQL_FIND_ALL_CAR_DESCRIPTIONS
            + " WHERE quality_class_id = ? ";
    public static final String SQL_FIND_CAR_DESCRIPTIONS_BY_BRAND_ID = SQL_FIND_ALL_CAR_DESCRIPTIONS
            + " WHERE brand_id = ? ";
    public static final String SQL_INSERT_CAR_DESCRIPTION = "INSERT INTO `car_description` "
            + " (`id`, `brand_id`, `quality_class_id`, `price`, `total_count`, `model`, "
            + "`coupe_type`, `generation`, `power`, `fuel_type`, `description`, `image`)"
            + " VALUES (DEFAULT,  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)";
    public static final String SQL_UPDATE_CAR_DESCRIPTION = "UPDATE  `car_description` "
            + " SET `brand_id`= ?, `quality_class_id`= ?, `price`= ?, `total_count`= ?, `model`= ?, "
            + "`coupe_type`= ?, `generation`= ?, `power`= ?, `fuel_type`= ?, `description`= ?, `image`= ? WHERE id = ?";
    /**
     * SQL queries to `car` table
     **/

    public static final String SQL_DELETE_CAR = "DELETE FROM car WHERE id = ?";
    public static final String SQL_INSERT_CAR = "INSERT INTO `car` (`id`, `car_number`, `car_description_id`, `description`, "
            + "`parking_id`, `status`)" + " VALUES (DEFAULT, ? ,?, ?, ?, DEFAULT)";
    public static final String SQL_UPDATE_CAR = "UPDATE `car` SET `car_number`= ?, "
            + " `description`= ?, `parking_id` =? WHERE id =?";
    public static final String SQL_FIND_ALL_CARS = "SELECT * FROM car c" + " JOIN parking p ON c.parking_id = p.id ";
    public static final String SQL_FIND_CARS = SQL_FIND_ALL_CARS + " WHERE car_description_id = ?";
    public static final String SQL_FIND_CAR_BY_ID = SQL_FIND_ALL_CARS
            + " JOIN car_description cd ON cd.id = c.car_description_id " + "WHERE c.id = ?";

    public static final String SQL_UPDATE_CAR_STATUS = "UPDATE `car` SET `status`= ? WHERE `id`= ? ";
    public static final String SQL_UPDATE_CAR_PARKING = "UPDATE `car` SET `parking_id`= ? WHERE `id`= ? ";
    public static final String SQL_GET_ORDER_CARS = "SELECT * FROM orders_cars oc" + " JOIN car c ON c.id = oc.car_id "
            + "JOIN parking p ON p.id = c.parking_id " + " where order_id = ? ";
    /**
     * SQL queries to `order` table
     **/
    public static final String SQL_FREE_CHECK = "SELECT * FROM  orders_cars  oc JOIN `order` o "
            + " ON oc.order_id = o.id WHERE (oc.car_id = ? AND o.status!='rejected') AND "
            + " ((?  between o.pick_up_date AND o.drop_off_date ) "
            + "			OR ( ?  between o.pick_up_date AND  o.drop_off_date )"
            + "OR  (? <= o.pick_up_date AND  ?  >= o.drop_off_date ))";
    public static final String SQL_GET_ALL_ORDERS = "SELECT * FROM `order` ";
    public static final String SQL_FIND_ORDER_BY_ID = SQL_GET_ALL_ORDERS + " WHERE  id = ? ";
    public static final String SQL_INSERT_ORDER = "INSERT INTO `order` "
            + " (`id`,`status`, `user_id`, `pick_up_date`, `drop_off_date`, `driver`, "
            + " `description`)" + " VALUES (default,default,?,?,?,?,?)";

    public static final String SQL_INSERT_ORDERS_CARS = "INSERT INTO orders_cars"
            + " (`order_id`, `car_id`, `payment_id`) VALUES (?, ?, ?);";
    public static final String SQL_UPDATE_ORDER_STATUS = "UPDATE `order`" + " SET `status`=?, `comment`=? WHERE `id`=?";
    public static final String SQL_GET_USER_ORDERS = SQL_GET_ALL_ORDERS + " WHERE user_id = ?";

    /**
     * SQL queries to `card` table
     **/
    public static final String SQL_INSERT_CARD = "INSERT INTO `card`"
            + " (`id`, `number`, `holder`, `CVV2`, `expiration_date`, `type`)" + " VALUES (DEFAULT, ?, ?, ?, ?, ?)";

    /**
     * SQL queries to `payment` table
     **/
    public static final String SQL_INSERT_PAYMENT = "INSERT INTO `payment` "
            + "(`card_id`,`id`,  `price`, `cash`, `description`) " + "VALUES ( ?,DEFAULT, ?, ?, ?)";
    public static final String SQL_INSERT_PAYMENT_CASH = "INSERT INTO `payment` "
            + "(`card_id`,`id`,  `price`, `cash`, `description`) " + "VALUES (DEFAULT,DEFAULT,?, ?, ?)";
    public static final String SQL_GET_PAYMENT_BY_ORDER_ID = "SELECT * FROM  payment p JOIN orders_cars oc"
            + " ON oc.payment_id = p.id	WHERE oc.order_id = ?";

    /**
     * Complex query : filters cars by price(minimal an max), parking_id, brand_id,
     * also checks if car is busy in certain dates. Used in SearchListCommand.
     * Instruction for PreparedStatement: 1 - minimal price(double), 2 - max
     * price(double), 3 - parking_id (or '%' if any parking), 4 - brand_id (or '%'
     * if any brand), 5 - quality_class_id(or '%' if quality class), 6 and 8 -
     * pick-up date-time, 7 and 9 - drop-off date-time.
     **/
    public static final String SQL_SEARCH_CARS = "SELECT * FROM cars.car c JOIN parking p " + "ON c.parking_id = p.id "
            + "LEFT JOIN orders_cars oc ON oc.car_id = c.id " + "LEFT JOIN `order` o  ON oc.order_id = o.id "
            + "JOIN car_description cd ON cd.id = c.car_description_id " + " WHERE ( cd.price>=? AND cd.price<=?  "
            + "AND c.parking_id LIKE ?  " + "AND cd.brand_id LIKE ? AND cd.quality_class_id LIKE ?)  "
            + "AND ((o.id = null) OR  (SELECT count(oc.car_id) FROM  orders_cars  oc JOIN `order` o"
            + "  ON oc.order_id = o.id WHERE (oc.car_id = c.id AND o.status!='rejected') AND "
            + " ((?  between o.pick_up_date AND o.drop_off_date ) "
            + "OR (? between o.pick_up_date AND  o.drop_off_date ) OR "
            + " (? <= o.pick_up_date AND ? >= o.drop_off_date )))=0);";

    /**
     * Private constructor, do nothing (prevents the creation of a new instance)
     **/
    private Queries() {
    }
}
