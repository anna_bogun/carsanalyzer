package ua.nure.anna.bohun.car.analyzer.api;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ua.nure.anna.bohun.car.analyzer.persistence.entity.Order;
import ua.nure.anna.bohun.car.analyzer.persistence.repository.OrderRepository;

@RestController
@RequiredArgsConstructor
public class OrderController {

    private final OrderRepository orderRepository;

    @RequestMapping(value = "/orders/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Order getOrderById(@PathVariable("id") Integer id) {
        return orderRepository.findById(id).get();
    }

    @RequestMapping(value = "/orders/{id}", method = RequestMethod.DELETE)
    public void deleteById(@PathVariable("id") Integer id) {
        orderRepository.deleteById(id);
    }

    @RequestMapping(value = "/orders/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<Order> getAll() {
        return orderRepository.findAll();
    }
}
