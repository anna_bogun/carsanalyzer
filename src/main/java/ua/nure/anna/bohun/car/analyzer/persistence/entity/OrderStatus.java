package ua.nure.anna.bohun.car.analyzer.persistence.entity;

/**
 * Enumeration, contains order statuses:newed, inprogress, completed, rejected.
 * Use in {@link Order}
 **/
public enum OrderStatus {
    newed, inprogress, completed, rejected
}