package ua.nure.anna.bohun.car.analyzer.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import ua.nure.anna.bohun.car.analyzer.persistence.entity.CarModel;

@Data
@AllArgsConstructor
public class RecommendationKnowledge {
    private Integer carId;

    private Boolean isRecommended;
}
