package ua.nure.anna.bohun.car.analyzer.persistence.repository;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.nure.anna.bohun.car.analyzer.persistence.entity.Client;

@Repository
public interface ClientRepository extends PagingAndSortingRepository<Client, Integer> {
    @Query(value = "SELECT * FROM client WHERE id <> :id  AND male = :male " +
            "ORDER BY ABS( DATEDIFF( birthdate, (SELECT c.birthdate FROM client c WHERE c.id = :id))) LIMIT 1",
            nativeQuery = true)
    Client findNearestClientByIdAndMale(@Param("id") Integer id, @Param("male") Boolean male);
}
