package ua.nure.anna.bohun.car.analyzer.persistence.repository;


import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ua.nure.anna.bohun.car.analyzer.persistence.entity.Order;

import java.util.List;

@Repository
public interface OrderRepository extends PagingAndSortingRepository<Order, Integer> {
    List<Order> findByClientId(Integer clientId);
}
