package ua.nure.anna.bohun.car.analyzer.model;

import lombok.Data;
import lombok.Getter;

import java.util.Map;
import java.util.TreeMap;

@Data
public class ClientPreference {

    private Map<Double, Double> priceRate = new TreeMap<>();
    private Map<String, Double> modelRate = new TreeMap<>();
    private Map<String, Double> brandRate = new TreeMap<>();
    private Map<Integer, Double> yearRate = new TreeMap<>();
    private Map<String, Double> bodyTypeRate = new TreeMap<>();
    private Map<String, Double> cylindersRate = new TreeMap<>();
    private Map<String, Double> engineSizeRate = new TreeMap<>();
    private Map<Integer, Double> horsepowerRate = new TreeMap<>();
    private Map<Integer, Double> valvesRate = new TreeMap<>();
    private Map<String, Double> driveTypeRate = new TreeMap<>();
    private Map<String, Double> transmissionRate = new TreeMap<>();
    private Map<String, Double> engineTypeRate = new TreeMap<>();
    private Map<String, Double> fuelTypeRate = new TreeMap<>();
    private Map<String, Double> countryOfOriginRate = new TreeMap<>();
    private Map<String, Double> carClassificationRate = new TreeMap<>();
}
