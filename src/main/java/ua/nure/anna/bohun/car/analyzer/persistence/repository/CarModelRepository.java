package ua.nure.anna.bohun.car.analyzer.persistence.repository;


import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ua.nure.anna.bohun.car.analyzer.persistence.entity.CarModel;

@Repository
public interface CarModelRepository
        extends PagingAndSortingRepository<CarModel, Integer> {
}