package ua.nure.anna.bohun.car.analyzer.api;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ua.nure.anna.bohun.car.analyzer.persistence.entity.CarModel;
import ua.nure.anna.bohun.car.analyzer.persistence.repository.CarModelRepository;

@RestController
@RequiredArgsConstructor
public class CarModelController {

    private final CarModelRepository carModelRepository;

    @RequestMapping(value = "/cars/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CarModel getOrderById(@PathVariable("id") Integer id) {
        return carModelRepository.findById(id).get();
    }

    @RequestMapping(value = "/cars/{id}", method = RequestMethod.DELETE)
    public void deleteById(@PathVariable("id") Integer id) {
        carModelRepository.deleteById(id);
    }

    @RequestMapping(value = "/cars/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<CarModel> getAll() {
        return carModelRepository.findAll();
    }
}
