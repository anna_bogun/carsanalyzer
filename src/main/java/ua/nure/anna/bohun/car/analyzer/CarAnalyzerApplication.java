package ua.nure.anna.bohun.car.analyzer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarAnalyzerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarAnalyzerApplication.class, args);
    }
}
