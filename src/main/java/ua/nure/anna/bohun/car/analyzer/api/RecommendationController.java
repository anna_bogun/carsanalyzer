package ua.nure.anna.bohun.car.analyzer.api;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ua.nure.anna.bohun.car.analyzer.model.Recommendation;
import ua.nure.anna.bohun.car.analyzer.service.RecommendationService;

import java.util.Map;

@RestController
@RequiredArgsConstructor
public class RecommendationController {

    private final RecommendationService recommendationService;

    @RequestMapping(value = "/recommend/{clientId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Recommendation recommend(@PathVariable Integer clientId, @RequestParam(required = false) Integer count) {
        return recommendationService.recommend(clientId, count);
    }

    @RequestMapping(value = "/predictions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<Integer, Map<Integer, Double>> predictions() {
        return recommendationService.predictions();
    }

    @RequestMapping(value = "/predictions/{clientId}/{count}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<Integer, Double> predictions(@PathVariable Integer clientId, @PathVariable Integer count) {
        return recommendationService.predictions(clientId, count);
    }
}