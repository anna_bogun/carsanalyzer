package ua.nure.anna.bohun.car.analyzer.persistence.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * CarDescription entity.
 **/
@Entity
@Table
@Data
public class CarModel implements BaseEntity{

    private static final long serialVersionUID = -6414837282271747215L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Integer id;

    @Column(nullable = false)
    private Double price;
    @Column(nullable = false)
    private String model;
    @Column(nullable = false)
    private String brand;
    @Column(nullable = false)
    private Integer year;
    @Column(nullable = false)
    private String bodyType;
    @Column(nullable = false)
    private String cylinders;
    @Column(nullable = false)
    private String engineSize;
    private Integer horsepower;
    private Integer valves;
    private String driveType;
    private String transmission;
    @Column(nullable = false)
    private String engineType;
    @Column(nullable = false)
    private String fuelType;
    @Column(nullable = false)
    private String countryOfOrigin;
    @Column(nullable = false)
    private String carClassification;
}