package ua.nure.anna.bohun.car.analyzer.persistence.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * Order entity
 **/
@Entity
@Table(name = "`order`")
@Data
public class Order implements BaseEntity {
    private static final long serialVersionUID = -1518423219557046837L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Integer id;

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "car_model_id", nullable = false)
    private CarModel carModel;


    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "client_id", nullable = false)
    private Client client;

    @Column(nullable = false)
    private Date date;

    @Column(nullable = false)
    private Date dropOffDate;

    @Column(nullable = false)
    private Date pickUpDate;

    @Column(nullable = false)
    private String status;

    @Column(nullable = false)
    private String carNumber;

    @Column(nullable = false)
    private Double rate;
}
