package ua.nure.anna.bohun.car.analyzer.util;

import org.apache.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Provides additional methods for application
 **/
public class Util {
    private static final Logger LOG = Logger.getLogger(Util.class);

    /**
     * @param date - String containing date in format "yyyy-MM-dd hh:mm:ss"
     * @returns Date as {@link Date}
     **/
    public static Date getDate(String date) {
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("yyyy-MM-dd hh:mm:ss");
        Date docDate = null;
        try {
            docDate = format.parse(date);
        } catch (ParseException e) {
            LOG.error("Error: cannot to parse date ==>" + date);
        }
        return docDate;
    }

    /**
     * @param Date as {@link Date}date
     * @returns - String containing date in format "yyyy-MM-dd hh:mm:ss"
     **/
    public static String getFormattedDate(Date date) {
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("yyyy-MM-dd hh:mm:ss");
        return format.format(date);
    }

    /**
     * @param Date as {@link Date}date
     * @returns - String containing date in format "yyyy-MM-dd"
     **/
    public static String getOnlyDate(Date date) {
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("yyyy-MM-dd");
        return format.format(date);
    }

    /**
     * @param Date as {@link Date}date
     * @returns - String containing date in format "hh:mm"
     **/
    public static String getOnlyTime(Date date) {
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("hh:mm");
        return format.format(date);
    }

    public static Date getMonth(String date) {
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("yyyy-MM");
        Date docDate = null;
        try {
            docDate = format.parse(date);
        } catch (ParseException e) {
            LOG.error("Error: cannot to parse month ==>" + date);
        }
        return docDate;
    }

    /**
     * @return days difference between two days
     **/
    public static long getDifferenceDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }
}
