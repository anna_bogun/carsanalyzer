package ua.nure.anna.bohun.car.analyzer.exception;


public class BadRequestAppException extends AppException {

    public BadRequestAppException() {
        super();
    }

    public BadRequestAppException(String message, Throwable cause) {
        super(message, cause);
    }

    public BadRequestAppException(String message) {
        super(message);
    }
}