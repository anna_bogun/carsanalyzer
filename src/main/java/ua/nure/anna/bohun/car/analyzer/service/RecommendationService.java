package ua.nure.anna.bohun.car.analyzer.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.IteratorUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ua.nure.anna.bohun.car.analyzer.exception.BadRequestAppException;
import ua.nure.anna.bohun.car.analyzer.model.ClientPreference;
import ua.nure.anna.bohun.car.analyzer.model.Recommendation;
import ua.nure.anna.bohun.car.analyzer.persistence.entity.CarModel;
import ua.nure.anna.bohun.car.analyzer.persistence.entity.Client;
import ua.nure.anna.bohun.car.analyzer.persistence.entity.Order;
import ua.nure.anna.bohun.car.analyzer.persistence.repository.CarModelRepository;
import ua.nure.anna.bohun.car.analyzer.persistence.repository.ClientRepository;
import ua.nure.anna.bohun.car.analyzer.persistence.repository.OrderRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class RecommendationService {
    private final OrderRepository orderRepository;

    private final ClientRepository clientRepository;

    private final CarModelRepository carModelRepository;

    private final CollaborativeFilteringService collaborativeFilteringService;

    public Map<Integer, Map<Integer, Double>> predictions() {
        List<CarModel> cars = IteratorUtils.toList(carModelRepository.findAll().iterator());
        List<Client> clients = IteratorUtils.toList(clientRepository.findAll().iterator());
        if (collaborativeFilteringService.isPredictionsEmpty()) {
            collaborativeFilteringService.buildPredictions(clients, cars);
        }
        return collaborativeFilteringService.getPredictions();
    }

    public Map<Integer, Double> predictions(Integer clientId, Integer count) {
        Map<Integer, Map<Integer, Double>> predictions = predictions();
        return predictions.get(clientId)
                .entrySet()
                .stream()
                .limit(count)
                .collect(Collectors.toMap(
                        entry -> entry.getKey(),
                        entry -> entry.getValue()));
    }

    public Recommendation recommend(Integer clientId, Integer count) {
        Optional<Client> clientOptional = clientRepository.findById(clientId);
        if (!clientOptional.isPresent()) {
            throw new BadRequestAppException("Client with id " + clientId + " was not found");
        }
        Client client = clientOptional.get();

        Recommendation recommendation = new Recommendation();
        recommendation.setClientId(client.getId());
        if (client.getOrders().isEmpty()) {
            log.info("No previous orders for client " + clientId + ". Prediction is started");
            Client nearestClient = clientRepository.findNearestClientByIdAndMale(clientId, client.getMale());
            predictions(nearestClient.getId(), count).entrySet().forEach(prediction -> recommendation
                    .addRecommendation(carModelRepository.findById(prediction.getKey()).get(),
                            prediction.getValue() > 0 ? true : false));
            return recommendation;
        }
        Double averageClientRate = client.getOrders().stream().mapToDouble(Order::getRate).average().getAsDouble();
        log.info("Average order rate for client " + clientId + " is " + averageClientRate);
        ClientPreference clientPreference = new ClientPreference();

        List<CarModel> cars = retrieveCars(count);
        client.getOrders()
                .stream()
                .forEach(order -> updateClientPreference(clientPreference, order));
        log.info("Car preference map for client" + clientId + " is " + clientPreference);
        Map<Integer, Double> carRates = new TreeMap<>();
        cars.forEach(car -> carRates.put(car.getId(), calculateCarRate(car, clientPreference)));

        carRates.entrySet().forEach(rate ->
                recommendation.addRecommendation(carModelRepository.findById(rate.getKey()).get(),
                        rate.getValue() > averageClientRate ? true : false));

        return recommendation;
    }

    private List retrieveCars(Integer count) {
        if (Objects.nonNull(count)) {
            return carModelRepository.findAll(PageRequest.of(1, count))
                    .get()
                    .collect(Collectors.toList());
        } else {
            return IteratorUtils.toList(carModelRepository.findAll().iterator());
        }
    }

    private void updateClientPreference(ClientPreference clientPreference, Order order) {
        CarModel car = order.getCarModel();
        Double rate = order.getRate();

        updateRate(clientPreference.getPriceRate(), car.getPrice(), rate);
        updateRate(clientPreference.getModelRate(), car.getModel(), rate);
        updateRate(clientPreference.getBrandRate(), car.getBrand(), rate);
        updateRate(clientPreference.getYearRate(), car.getYear(), rate);
        updateRate(clientPreference.getBodyTypeRate(), car.getBodyType(), rate);
        updateRate(clientPreference.getCylindersRate(), car.getCylinders(), rate);
        updateRate(clientPreference.getEngineSizeRate(), car.getEngineSize(), rate);
        updateRate(clientPreference.getHorsepowerRate(), car.getHorsepower(), rate);
        updateRate(clientPreference.getValvesRate(), car.getValves(), rate);
        updateRate(clientPreference.getDriveTypeRate(), car.getDriveType(), rate);
        updateRate(clientPreference.getTransmissionRate(), car.getTransmission(), rate);
        updateRate(clientPreference.getEngineTypeRate(), car.getEngineSize(), rate);
        updateRate(clientPreference.getFuelTypeRate(), car.getFuelType(), rate);
        updateRate(clientPreference.getCountryOfOriginRate(), car.getCountryOfOrigin(), rate);
        updateRate(clientPreference.getCarClassificationRate(), car.getCarClassification(), rate);
    }

    private <T> void updateRate(Map<T, Double> preferenceMap, T parameterValue, Double rate) {
        if (Objects.nonNull(parameterValue)) {
            if (!preferenceMap.containsKey(parameterValue)) {
                preferenceMap.put(parameterValue, Double.valueOf(rate));
            } else {
                preferenceMap.put(parameterValue, (preferenceMap.get(parameterValue) + rate) / 2);
            }
        }
    }

    private Double calculateCarRate(CarModel car, ClientPreference clientPreference) {
        Double rate = 0.0;
        rate += clientPreference.getPriceRate().getOrDefault(car.getPrice(), 0.0);
        rate += clientPreference.getModelRate().getOrDefault(car.getModel(), 0.0);
        rate += clientPreference.getBrandRate().getOrDefault(car.getBrand(), 0.0);
        rate += clientPreference.getYearRate().getOrDefault(car.getYear(), 0.0);
        rate += clientPreference.getBodyTypeRate().getOrDefault(car.getBodyType(), 0.0);
        rate += clientPreference.getCylindersRate().getOrDefault(car.getCylinders(), 0.0);
        rate += clientPreference.getEngineSizeRate().getOrDefault(car.getEngineSize(), 0.0);
        if (Objects.nonNull(car.getHorsepower())) {
            rate += clientPreference.getHorsepowerRate().getOrDefault(car.getHorsepower(), 0.0);
        }
        if (Objects.nonNull(car.getValves())) {
            rate += clientPreference.getValvesRate().getOrDefault(car.getValves(), 0.0);
        }
        if (Objects.nonNull(car.getDriveType())) {
            rate += clientPreference.getDriveTypeRate().getOrDefault(car.getDriveType(), 0.0);
        }
        if (Objects.nonNull(car.getTransmission())) {
            rate += clientPreference.getTransmissionRate().getOrDefault(car.getTransmission(), 0.0);
        }
        rate += clientPreference.getEngineTypeRate().getOrDefault(car.getEngineType(), 0.0);
        rate += clientPreference.getFuelTypeRate().getOrDefault(car.getFuelType(), 0.0);
        rate += clientPreference.getCountryOfOriginRate().getOrDefault(car.getCountryOfOrigin(), 0.0);
        rate += clientPreference.getCarClassificationRate().getOrDefault(car.getCarClassification(), 0.0);
        return rate / 15;
    }
}
