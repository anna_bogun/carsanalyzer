package ua.nure.anna.bohun.car.analyzer.api;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ua.nure.anna.bohun.car.analyzer.persistence.entity.Client;
import ua.nure.anna.bohun.car.analyzer.persistence.repository.ClientRepository;

@RestController
@RequiredArgsConstructor
public class ClientController {

    private final ClientRepository clientRepository;

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Client getOrderById(@PathVariable("id") Integer id) {
        return clientRepository.findById(id).get();
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.DELETE)
    public void deleteById(@PathVariable("id") Integer id) {
        clientRepository.deleteById(id);
    }

    @RequestMapping(value = "/clients/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<Client> getAll() {
        return clientRepository.findAll();
    }
}
