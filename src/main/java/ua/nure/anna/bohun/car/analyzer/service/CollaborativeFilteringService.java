package ua.nure.anna.bohun.car.analyzer.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ua.nure.anna.bohun.car.analyzer.persistence.entity.CarModel;
import ua.nure.anna.bohun.car.analyzer.persistence.entity.Client;
import ua.nure.anna.bohun.car.analyzer.persistence.entity.Order;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

@Service
@Slf4j
public class CollaborativeFilteringService {
    private Map<Integer, Map<Integer, Double>> differences = new HashMap<>();
    private Map<Integer, Map<Integer, Integer>> frequencies = new HashMap<>();
    private Map<Integer, Map<Integer, Double>> predictions = new HashMap<>();

    public Boolean isPredictionsEmpty() {
        return predictions.isEmpty();
    }

    public void buildPredictions(List<Client> clients, List<CarModel> cars) {
        log.info("Collaborative Filtering - Before the Prediction");
        Map<Client, Map<CarModel, Double>> inputData = new HashMap<>();
        for (Client client : clients) {
            Map<CarModel, Double> orders = new HashMap<>();
            for (Order order : client.getOrders()) {
                orders.put(order.getCarModel(), order.getRate());
            }
            inputData.put(client, orders);
        }
        buildDifferencesMatrix(inputData);
        predict(inputData, cars);
    }

    public Map<Integer, Map<Integer, Double>> getPredictions() {
        return predictions;
    }

    /**
     * Based on the available data, calculate the relationships between the
     * cars and number of occurences
     *
     * @param data existing client data and their cars' ratings
     */
    private void buildDifferencesMatrix(Map<Client, Map<CarModel, Double>> data) {

        for (Map<CarModel, Double> client : data.values()) {
            for (Entry<CarModel, Double> carRate : client.entrySet()) {
                if (!differences.containsKey(carRate.getKey().getId())) {
                    differences.put(carRate.getKey().getId(), new HashMap<Integer, Double>());
                    frequencies.put(carRate.getKey().getId(), new HashMap<Integer, Integer>());
                }
                for (Entry<CarModel, Double> secondCarRate : client.entrySet()) {
                    int oldCount = 0;
                    if (frequencies.get(carRate.getKey().getId()).containsKey(secondCarRate.getKey().getId())) {
                        oldCount = frequencies.get(carRate.getKey().getId()).get(secondCarRate.getKey().getId()).intValue();
                    }
                    double oldDiff = 0.0;
                    if (differences.get(carRate.getKey().getId()).containsKey(secondCarRate.getKey().getId())) {
                        oldDiff = differences.get(carRate.getKey().getId()).get(secondCarRate.getKey().getId()).doubleValue();
                    }
                    double observedDiff = carRate.getValue() - secondCarRate.getValue();
                    frequencies.get(carRate.getKey().getId()).put(secondCarRate.getKey().getId(), oldCount + 1);
                    differences.get(carRate.getKey().getId()).put(secondCarRate.getKey().getId(), oldDiff + observedDiff);
                }
            }
        }
        for (Integer j : differences.keySet()) {
            for (Integer i : differences.get(j).keySet()) {
                double oldValue = differences.get(j).get(i).doubleValue();
                int count = frequencies.get(j).get(i).intValue();
                differences.get(j).put(i, oldValue / count);
            }
        }
    }

    /**
     * Based on existing data predict all missing ratings. If prediction is not
     * possible, the value will be equal to -1
     *
     * @param data existing client data and their cars' ratings
     */
    private void predict(Map<Client, Map<CarModel, Double>> data,
                         List<CarModel> cars) {
        Map<Integer, Double> uPred = new HashMap<Integer, Double>();
        Map<Integer, Integer> uFreq = new HashMap<Integer, Integer>();
        for (Integer j : differences.keySet()) {
            uFreq.put(j, 0);
            uPred.put(j, 0.0);
        }
        for (Entry<Client, Map<CarModel, Double>> e : data.entrySet()) {
            for (CarModel j : e.getValue().keySet()) {
                for (Integer k : differences.keySet()) {
                    try {
                        double predictedValue = differences.get(k).get(j).doubleValue() + e.getValue().get(j).doubleValue();
                        double finalValue = predictedValue * frequencies.get(k).get(j).intValue();
                        uPred.put(k, uPred.get(k) + finalValue);
                        uFreq.put(k, uFreq.get(k) + frequencies.get(k).get(j).intValue());
                    } catch (NullPointerException e1) {
                    }
                }
            }
            Map<Integer, Double> clean = new HashMap<Integer, Double>();
            for (Integer j : uPred.keySet()) {
                if (uFreq.get(j) > 0) {
                    clean.put(j, uPred.get(j).doubleValue() / uFreq.get(j).intValue());
                }
            }
            for (CarModel j : cars) {
                if (e.getValue().containsKey(j)) {
                    clean.put(j.getId(), e.getValue().get(j));
                } else if (!clean.containsKey(j)) {
                    clean.put(j.getId(), -1.0);
                }
            }
            predictions.put(e.getKey().getId(), clean);
        }
        printData(predictions);
    }

    private void printData(Map<Integer, Map<Integer, Double>> data) {
        for (Integer clientId : data.keySet()) {
            log.info(clientId + "(clientId):");
            print(data.get(clientId));
        }
    }

    private void print(Map<Integer, Double> hashMap) {
        NumberFormat formatter = new DecimalFormat("#0.000");
        for (Integer j : hashMap.keySet()) {
            log.info(" " + j + " --> " + formatter.format(hashMap.get(j).doubleValue()));
        }
    }
}