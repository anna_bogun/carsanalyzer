package ua.nure.anna.bohun.car.analyzer.exception;

/**
 * An exception that provides information on an application error.
 **/
public class AppException extends RuntimeException {

    private static final long serialVersionUID = 1438506368427751752L;

    public AppException() {
        super();
    }

    public AppException(String message, Throwable cause) {
        super(message, cause);
    }

    public AppException(String message) {
        super(message);
    }

}