# СarsAnalyzer

This application is designed for calculating recommendations for car rental clients.

## Used technologies

1) Java 8
2) Gradle 6.3
3) Spring Boot + Spring Data
4) MySql
5) Junit 4
6) Lombok

### API
Swagger link with endpoints after local start:
 http://localhost:8083/swagger-ui.html#/
